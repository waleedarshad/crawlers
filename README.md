# zig-crawlers

This project contains all of the site crawlers and scrapers for zig.com. Each crawler will be run periodically to capture all new images from each site.

## Setup

* Install Ruby version 2.1.3
* Run `bundle install`
* Copy "env.sample" to ".env"

That's it!

## Crawlers

Put all crawlers in the `./crawlers` directory. There should be one site per file, and its class name should match the file name. Ex: `./crawlers/us_weekly.rb` defines a class named `UsWeekly`.

Each crawler should be a class that responds to a `#crawl` method. This method should return an array of all of the newly crawled images. All de-duping of the images will be handled outside the classes, so you can simply return all images every time without needing to worry about duplication.

The array of results should contain a hash, just like the following example:

```ruby
{
  source: "https://site.com/page",
  title: "The Title for the Image",
  path: "https://site.com/image.jpg",
  caption: "The caption to display with this photo",
  people: "a string of content that may contain names"
}
```

The keys for the hash are:

* `source` (String) – The url of the page where the image was captured from.
* `title` (String) - The title of the image. Usually the headline it is displayed next to.
* `path` (String) - The url of the image source itself. This should be a jpg file path.
* `caption` (String) - The text to display under the image itself. This may be long.
* `people` (String) - A string containing any other names that may be referenced in the photo. For example, if there are celebrity names elsewhere on a page that relate to the photo they may be displayed here. This is optional, but helps to categorize photos if provided.

## Running a Crawler

You can run a crawler by using the rake task:

```bash
bundle exec rake crawl
```

With no variables, this task will look at the `crawlers` directory and ask you which one to run. Type the name of the file at the prompt to run.

If you pass an environment variable named `CRAWLER` to the command, it will skip the prompt and run the crawler you give it. For example:

```bash
CRAWLER=us_weekly bundle exec rake crawl
```

The output of the crawler should always be an array of the images grabbed.

## Production Setup

To have the crawler actually crawl and update the production site, set the `ZIG_BASE_URL` to point to the [receiver URL](http://zig.herokuapp.com/api/a/photos) and the production `AUTH_TOKEN`.

Then, update the `config/schedule.rb` file to put the particular crawl on a regular schedule. The syntax is from the [whenever gem](https://github.com/javan/whenever). The production crawl wrapper is in `bin/zig-crawler`. This calls the same base classes as the rake task, but when complete dumps the results over to production.

# Debugging

Pry is your friend. Put `binding.pry` anywhere in a class to debug.

Also, just use `bundle console` for editing directly if that's easier:

```bash
bundle console
```

Then:

```ruby
require "./crawlers/us_weekly"

c = UsWeekly.new
results = c.crawl
```

This will get you an interactive shell of the results.
