class HollywoodLife < Zig::Crawler
  self.site_key     = 'hollywoodlife.com'
  self.site_root    = 'http://www.hollywoodlife.com'
  self.max_attempts = 10

  protected

  def process_pages

    visit(site_root)

    urls = get_article_urls

    urls.each do |url|
      visit(url)


      if session.first('.interstitial')
        p "INTERSTITIAL FOUND"
        session.find("#continue-link").click
      end
      
      series_title        = title
      series_published_at = published_at

      if article_image
        add_result do |result|
          result.source       = session.current_url
          result.title        = title
          result.published_at = series_published_at
          result.caption      = get_series_caption
          result.people       = pull_people(get_series_caption)
          result.series_root  = url if article_has_slideshow
          result.series_item  = 0 if article_has_slideshow
          result.path         = article_image[:src]
        end
      end

      if article_has_slideshow
        visit(slideshow_url)
        @index = 1

        total_slides = slides_count

        until @index > total_slides
            add_result do |result|
              result.source       = session.current_url
              result.title        = get_slide_caption
              result.published_at = series_published_at
              result.caption      = get_slide_caption
              result.people       = pull_people(get_slide_caption)
              result.series_root  = url
              result.series_item  = @index
              result.path         = slide_image
            end
          visit(session.current_url.gsub("#!#{@index}", "#!#{@index + 1}"))
          sleep 1
          @index += 1
        end
      end
    end
  end

  def get_article_urls
    session.all('div.hl-post a.hl-hp-link').collect{|link| link[:href]}.select{|url| URI(url).host =~ /hollywoodlife.com/}
  end

  def title
    session.find('section.main-post > header > h1').text
  rescue
    binding.pry
  end

  def published_at
    session.first('div.meta > span.posted').text.split(" by")[0] || session.first('div.meta > span.posted').text
  end

  def article_image
    session.first('article.entry-content > a > img')
  end

  def get_series_caption
    session.first('article.entry-content h3').text
  end

  def get_slide_caption
    session.find('.gallery-caption').text || ""
  end

  def article_has_slideshow
    !slideshow_url.nil? && URI(slideshow_url).host == 'hollywoodlife.com'
  end

  def slideshow_url
    session.first('article.entry-content > a').nil? ? nil : session.first('article.entry-content > a')[:href]
  end

  def slide_image
    session.first('div.image-current > img')[:src]
  end

  def slides_count
    session.find('div.gallery-image > .gallery-multi').all('div')[1..-2].count
  end

end
