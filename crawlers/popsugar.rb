class Popsugar < Zig::Crawler
  self.site_key     = 'popsugar.com'
  self.site_root    = 'http://www.popsugar.com/celebrity'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url + "?stream_view=1")

      sleep 2

      close_annoying_popup

      image_containers.each_with_index do |figure, index|
        if figure.first('img.image.xxxlarge')
    
        add_result do |result|
          result.title        = figure['data-title'] || title
          result.source       = figure['data-permalink'] || url
          result.path         = figure.first('img.image.xxxlarge')[:src]
          result.caption      = description
          result.published_at = published_at
          result.people       = pull_people(result.title + ' ' + result.caption)
          result.series_root  = url   unless @containers.count == 1
          result.series_item  = index unless @containers.count == 1
        end
      end
      end
      @containers = nil
      session.driver.reset!
    end
  end


  def gallery_urls
    session.all('div.post-image.no-hover-share a').collect{|link| link[:href]}.uniq
  end

  def image_containers
    @containers ||= session.all('article > figure').reject{|f| f[:class] =~ /ad/}
  end

  def number_of_slides
    session.first('div.slide-count').text.split(' OF ')[1].split(' View On One Page').first.to_i
  end

  def number_slides_remaining
    number_of_slides - 1
  end

  def image
    session.first('div.contents img')
  end

  def caption
    session.first('figcaption').try(:text)
  end

  def description
    session.first('h2.post-title span.title-text').try(:text)
  end

  def title
    session.first('span.title-text').try(:text)
  end

  def published_at
    session.first('.post-meta time')[:title]
  end

  def close_annoying_popup
    session.first('#poeoverlay-close').try(:click)
  end
end
