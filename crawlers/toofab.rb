class Toofab < Zig::Crawler
  self.site_key = 'toofab.com'
  self.site_root = 'http://toofab.com/'
  self.max_attempts = 20

  protected

  def process_pages
    visit(site_root)

    article_urls.each do |url|
      visit(url)

      is_slideshow? ? crawl_gallery(url) : crawl_article(url)
    end

  end

  def crawl_article(url)
    article_images.each_with_index do |image,index|
      add_result do |result|
        result.title = article_title
        result.source = session.current_url
        result.path = image[:src]
        result.caption = image[:alt]
        result.published_at = published_at
        result.people = pull_people(result.title + ' ' + result.caption)
        result.series_item = index+1
        result.series_root = url
      end

    end
  end

  def is_slideshow?
    session.first('div.sf_gallery') != nil
  end

  def article_urls
    urls = []
    session.all('div.list-group article a').take(8).map { |link| urls << link[:href] }.uniq
    session.all('article.photo-gallery a').take(8).map { |link| urls << link[:href] }.uniq
    urls
  end

  def article_images
    session.all('div.page-content img.img-responsive')
  end

  def article_title
    session.first('h1.article-header').try(:text)
  end

  def gallery_count
    session.first('p.image_count').try(:text).split('of')[1].to_i
  end

  def crawl_gallery(url)
    gallery_count.times do |index|
      next if image.nil?
      add_result do |result|
        result.title = title
        result.source = session.current_url
        result.path = image[:src]
        result.caption = caption
        result.published_at = "#{Date.today}"
        result.people = pull_people(result.title + ' ' + result.caption)
        result.series_item = index+1
        result.series_root = url
      end
      click_next_slide
      sleep 1
    end
  end

  def click_next_slide
    session.first('div.controls a.next').trigger(:click)
  end

  def image
    session.first('div.image_wrapper img.image')
  end

  def title
    session.first('div.controls p.bar').try(:text)
  end

  def caption
    session.first('div.metadata p.description').try(:text)
  end

  def published_at
    session.first('span.publish-date time').try(:text)
  end
end
