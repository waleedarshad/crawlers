class UsaToday < Zig::Crawler
  self.site_key     = 'usatoday.com'
  self.site_root    = 'http://www.usatoday.com/media/latest/photos/life/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)
    gallery_urls.each do |url|
      visit(url)

      gallery_images.each_with_index do |div, index|

        if image(div) && description(div)
          add_result do |result|
            result.title        = title
            result.source       = session.current_url
            result.path         = image(div)['data-src'] == nil ? image(div)[:src] : image(div)['data-src']
            result.caption      = description(div)
            result.published_at = published_at
            result.people       = pull_people([result.title,result.caption].join(' '))
            result.series_root  = url
            result.series_item  = index+1
          end
        end
      end
    end

  end

  def gallery_urls
    session.all('ul.grid.media-grid-ul li a.media-list-link').map { |link| link[:href] }.uniq.take(8)
  end

  def gallery_images
    session.all('div.gallery-photo-border', :visible => false)
  end

  def title
    session.first('div.title.cinematic-headline-div').try(:text)
  end

  def image(div)
    div.first('img.gallery-photo', :visible => false) || div.first('img.gallery-photo', :visible => true)
  end

  def description(div)
    description = div.find('span.js-caption', :visible => false).try(:text)
    description == '' ? image(div)[:alt] : description
  end

  def published_at
    session.first('div.date.cinematic-headline-div').try(:text)
  end
end
