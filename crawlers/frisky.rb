class Frisky < Zig::Crawler
  self.site_key     = 'thefrisky.com'
  self.site_root    = 'http://www.thefrisky.com/celebs/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    article_urls.each do |url|
      visit(url)

      images.each_with_index do |image,index|
        if image
          add_result do |result|
            result.title    = title
            result.source   = session.current_url
            result.path     = image[:src]
            result.caption  = image[:alt]
            result.published_at  = published_at
            result.people   = pull_people(result.title + ' ' + result.caption)
            result.series_root  = url
            result.series_item  = index+1
          end
        end
      end
    end
  end

  def article_urls
    session.all('div.feed .post .image-holder a').take(10).map { |link| link[:href] }
  end

  def images
    session.all('div.article-body div.img-wrap img')
  end

  def title
    session.first('div.article-body h1').try(:text)
  end

  def published_at
    session.first('span.date').try(:text).split('-')[0].gsub('|','')
  end
end
