class InstagramApi < Zig::Crawler
  self.site_key   = 'instagram.com'
  self.site_root  = 'https://instagram.com'

  protected

  def process_pages
    media.each do |instance|
      next unless instance.type == 'image'

      add_result do |result|
        result.title          = instance.user.username
        result.caption        = instance.caption.try(:text)
        result.source         = instance.link
        result.path           = instance.images.standard_resolution.url
        result.people         = instance.user.username
        result.published_at   = Time.zone.at(instance.created_time.to_i)
        result.width          = instance.images.standard_resolution.width
        result.height         = instance.images.standard_resolution.height
      end
    end
  end

  def client
    @client ||= Instagram.client
  end

  def media
    @media ||= client.user_media_feed(count: 25).reverse
  end
end
