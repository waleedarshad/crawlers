class JustJared < Zig::Crawler
  self.site_key     = 'justjared.com'
  self.site_root    = 'http://justjared.com'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    post_urls.each do |url|
      visit(url)
      # sleep 1

      next if images.count <= 0

      series = true if images.count > 1
      images.each_with_index do |link, index|
        if link
          visit(link)

          add_result do |result|
            result.title    = title
            result.source   = session.current_url
            result.path     = image[:src]
            result.caption  = description
            result.published_at  = published_at
            result.people   = pull_people(result.title + ' ' + image[:alt])
            result.series_root = url if series
            result.series_item = index+1 if series
          end
        end
      end
    end
  end

  def post_urls
    session.all('div#content div.post a[rel=bookmark]').take(12).map { |link| link[:href] }
  end

  def images
    session.all('div#content div.post div.minigallery div.tnlist div.tn a').take(12).map { |link| link[:href] }
  end

  def image
    session.first('div#maincontent div.mainphoto img#feature_image')
  end

  def title
    session.first('div#maincontent h1').try(:text)
  end

  def description
    session.first('div.mainphoto p.contentsnippet').try(:text)
  end

  def published_at
    session.first('div#maincontent div.photo-date').try(:text).split('AT')[0]
  end
end