require 'open-uri'
require 'rss'
class PeopleMag < Zig::Crawler
  self.site_key     = 'people.com'
  self.site_root    = 'http://www.people.com/people/'
  self.max_attempts = 100

  protected

  def process_pages
    visit(site_root)

    article_urls.each do |url|
      visit(url)

      next unless main_image

      add_result do |result|
        result.title        = main_image_alt
        result.source       = session.current_url
        result.path         = main_image_src
        result.caption      = article_description
        result.published_at = published_at
        result.people       = pull_people([result.title, result.caption].join(" "))
      end

      image_list.each_with_index do |image, index|
        if image
          add_result do |result|
            result.title        = title(image)
            result.source       = session.current_url
            result.path         = image(image)
            result.caption      = image_description(image)
            result.published_at = published_at
            result.people       = pull_people([result.title, result.caption].join(" "))
            result.series_root  = url
            result.series_item  = index+1
          end
        end
      end
    end
  end

  def article_urls
    session.all('div.tout-latest-news div.imgcont a').map{|link| link[:href]}
  end

  def article_description
    session.first('div#articleHeader h1').try(:text)
  end

  def main_image
    session.first('div#mainPhoto div.image img')
  end

  def main_image_alt
    if main_image
      main_image[:alt]
    end
  end

  def main_image_src
    if main_image
      main_image[:src]
    end
  end

  def image_list
    session.all('div#leftCol div#articleBody div.imgcont')
  end

  def image(div)
    div.first('img')[:src]
  end

  def title(div)
    div.first('img')[:alt]
  end

  def image_description(div)
    div.first('div p.caption').try(:text)
  end

  def published_at
    session.first('div#article_details p.published abbr.timestamp').try(:text)
  end
end
