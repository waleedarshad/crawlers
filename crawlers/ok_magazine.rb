class OkMagazine < Zig::Crawler
  self.site_key     = 'okmagazine.com'
  self.site_root    = 'http://okmagazine.com/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      slides = get_slides

      index = 1
      slides.each do |slide|

        if image_for_slide(slide)
          add_result do |result|
            result.title        = title
            result.source       = slide["data-item-url"]
            result.path         = image_for_slide(slide)
            result.caption      = description_for_slide(slide)
            result.published_at = published_at
            result.people       = pull_people([title, description_for_slide(slide)].join(" "))
            result.series_root  = url
            result.series_item  = index
          end
          index += 1
        end
      end
    end
  end

  def gallery_urls
    session.all('a', text: /pics/i).map do |link|
      link[:href]
    end.uniq.take(12)
  end

  def get_slides
    session.all('#gallery-container .owl-item').to_a.collect{|div| div.first('div')}.reject{|div| div[:class] =~ /netseer|taboola|embed/}
  end

  def slides_count
    session.first('span.ami-total').text.to_i
  end

  def image_for_slide(slide)
    begin
      slide.first('img')["data-src"].presence || slide.first('img').try(:src)
    rescue
      
    end
  end

  def gallery_content
    session.first('.owl-item.active')
  end

  def title
    session.first('.entry-title').try(:text)
  end

  def description_for_slide(slide)
    slide.first('.ami-gallery-entry').try(:text)
  end

  def published_at
    session.first('time[itemprop="datePublished"]').try(:text)
  end

  def click_next_slide
    session.first('.right-item').try(:click)
  end
end
