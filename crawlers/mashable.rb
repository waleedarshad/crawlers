class Mashable < Zig::Crawler
  self.site_key     = 'mashable.com'
  self.site_root    = 'http://www.mashable.com'
  self.max_attempts = 10

  protected

  def process_pages

    visit(site_root)

    urls = article_urls

    urls.each_with_index do |url, index|

      visit(url)

      if has_photos?
        if multiple_photos?

          unless inline_photos.empty?
            inline_photos.each do |img|
              add_result do |result|
                result.title        = article_title
                result.source       = session.current_url
                result.path         = img[:src]
                result.caption      = caption
                result.people       = pull_people(article_title + " " + caption)
                result.series_root  = session.current_url
                result.series_item  = index
                result.published_at = published_at
              end
              @index +=1
            end
          end
        else
          add_result do |result|
            result.title        = article_title
            result.source       = session.current_url
            result.path         = inline_photos[0][:src]
            result.caption      = caption
            result.people       = pull_people(article_title + " " + caption)
            result.series_root  = url
            result.series_item  = index
            result.published_at = published_at
          end
        end
      end
      session.driver.reset!
    end
  end

  def article_urls
    session.all('article.post div.article-img-container a').map{|art| art[:href]}.uniq.take(15)
  end

  def has_photos?
    !!(!inline_photos.empty?)
    # !!(!inline_photos.empty? || gallery_link)
  end

  def multiple_photos?
    !!(inline_photos.count > 1)
  end

  def inline_photos
    session.all('header.article-header figure.article-image div.microcontent-wrapper img')
  end

  def article_title
    session.all('header.article-header h1')[0].text
  end

  def caption
    session.all('header.article-header figure.article-image figcaption.image-caption')[0].text
  end

  def published_at
    session.all('header.article-header div.article-info time')[0][:datetime]
  end

  def image
    session.all('header.article-header figure.article-image div.microcontent-wrapper img') rescue nil
  end

  public

  def test_results
    self.results.count > 0
  end
end
