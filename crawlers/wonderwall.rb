class Wonderwall < Zig::Crawler
  self.site_key     = 'wonderwall.com'
  self.site_root    = 'http://www.wonderwall.com'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    story_links.each do |link|
      visit(link)

      sleep 1

      number_of_slides.times do |index|

        add_result do |result|
          result.title        = title
          result.source       = session.current_url
          result.path         = image
          result.caption      = caption
          result.series_root  = link
          result.series_item  = index+1
          result.published_at = published_at
          result.people       = pull_people(result.title + ' ' + result.caption)
        end

        click_next_slide
      end
    end

  end

  def story_links
    session.all('div.wall.stream div.wall-card span.img', :visible => false).take(1).collect{|a| a['data-href']}.compact
  end

  def title
    session.first('div.slide.is-selected div.img-wrap img', :visible => false)[:alt]
  end

  def caption
    session.first('div.gallery-body-wrap div.description-destination p', :visible => false).text(:all)
  end

  def image
    session.first('article .is-selected div.img-wrap', :visible => false)['data-img-src']
  end

  def number_of_slides
    session.first('div.meta-destination span.count', :visible => false).text(:all).split('/')[1].to_i-1
  end

  def published_at
    session.first('time.dateline', :visible => false)['datetime']
  end

  def click_next_slide
    visit(session.first('article .is-selected div.img-wrap .slide-arrows .next', :visible => false)[:href])
  end

end
