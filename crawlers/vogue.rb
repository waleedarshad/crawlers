class Vogue < Zig::Crawler
  self.site_key     = 'vogue.com'
  self.site_root    = 'http://www.vogue.com'
  self.max_attempts = 10

  protected

  def process_pages

    section_paths.each do |path|

      visit(site_root + path)


      article_element_urls.each do |url|

        visit(url)

        if !!(url =~ /slideshow/) && slideshow_image_count
          
          process_voting_slideshow(url, slideshow_image_count)
        
        elsif session.first('.gallery__inline')
          
          if session.first('.gallery__inline')["data-gallery_slides"]
            parsed_json = JSON.parse(session.first('.gallery__inline')["data-gallery_slides"])
            process_inline_gallery_via_json(parsed_json)
          end
        elsif session.first(".slide-show--slide--picture")
             
            _img = session.find(".expanded-image--wrapper img.picture-img")
            url = _img[:src].blank? ? _img[:src] : _img[:srcset]
            slides_count = slideshow_image_count
            this_title = title
            visit(url)
            process_voting_slideshow(url, slides_count, this_title)

        elsif session.first('.content-image--wrapper .gallery-marker--icon')
          # debugger
          
          session.first(".content-image--wrapper img.picture-img").trigger(:click)
          # debugger
          # url = _img[:src].blank? ? _img[:src] : _img[:srcset]
          # slides_count = slideshow_image_count
          # this_title = title
          # visit(url)
          process_voting_slideshow(url, slideshow_image_count)
        
        # elsif session.first('div.listicle')
        #   debugger
        #   listicle_slides.each_with_index do |item, index|
        #     add_result do |result|
        #       result.source       = session.current_url
        #       result.series_root  = session.current_url
        #       result.series_item  = index + 1
        #       result.title        = index == 0 ? title : listicle_slide_title(item)
        #       result.path         = listicle_slide_image(item)
        #       result.caption      = listicle_slide_caption(item)
        #       result.people       = pull_people(listicle_slide_caption(item))
        #       result.published_at = published_at
        #     end
        #   end
        else
          
          process_single_article(url)
        end
      end
    end

  end

  def section_paths
    ['/fashion/celebrity-style/', '/beauty/celebrity-beauty/']
  end

  def article_element_urls
    
    session.all('.feed-card .feed-card--image a').map { |link| link[:href] }.uniq.take(8)
  end

  def process_voting_slideshow(root_url, slides_count, parent_article_title = nil)
    
    index = 0
    total_slide_count = slides_count || session.first('.gallery--counter--total').text[/\d+/].to_i
      slideshow_slide.each_with_index do |slide,n|
      
      add_result do |result|
        result.source       = session.current_url
        result.series_root  = root_url
        result.series_item  = n+1
        result.title        = n == 0 ? parent_article_title || slideshow_title(slide) || slideshow_caption(slide) : slideshow_title(slide) || slideshow_caption(slide)
        result.path         = slideshow_image_path(slide)
        result.caption      = slideshow_caption(slide)
        result.people       = slideshow_people(slide) || pull_people([parent_article_title, slideshow_title(slide), slideshow_caption(slide)].compact.join(" "))
        result.published_at = slideshow_published_at
      end
      break if n == total_slide_count-1
    end
  end

  def process_inline_gallery_via_json(parsed_json)
    parsed_json.each_with_index do |slide, index|
      add_result do |result|
        result.source       = session.current_url
        result.series_root  = session.current_url
        result.series_item  = index + 1
        result.title        = title
        result.path         = slide['expandedImage']['sizes']['960']
        result.caption      = slide['caption']
        result.people       = [result.title,result.caption].join(' ')
        result.published_at = published_at
      end
    end
  end

  def process_single_article(url)
    add_result do |result|
      result.source = url
      result.title = title
      result.path = image_path
      result.caption = caption
      result.people  = people
      result.published_at = published_at
    end
  end

  def slideshow_image_count
    elem = session.first(".gallery-marker--count") || session.first(".gallery--counter--total")
    elem.nil? ? nil : elem.text[/\d{1,2}/].to_i
  end

  def title
    session.first('h1.article-content--title').try(:text)
  end

  def slideshow_title(article)

    article.first('h1.gallery--header--title').try(:text)
  end

  def image_path
    session.first('.content-image--wrapper source.picture-source-960')['srcset'].split(",")[0]
  end

  def slideshow_image_path(article)
    article.first('.gallery--slide--inner .gallery--slide--images source.picture-source-960',visible: false)['srcset'].split(", ")[0]
  end

  def caption # first p of article
    session.all('.article-copy--container p').reject{|p| p[:class]}[0].text
  end

  def slideshow_caption(article)

    article.first('.gallery--slide--caption').try(:text)
  end

  def people
    session.all('.article-tags--item').map{|li| li.text.split.map(&:capitalize)*' '}.to_sentence
  end

  def slideshow_people(article)

    slideshow_caption(article)[/WHO: (.*?) WHAT:/m, 1]
    rescue
      nil
  end

  def slideshow_slide # 1 based index
    
    session.all("article.gallery--slide.gallery--slide__image.carousel-slide")
    # session.all('article.gallery--slide.gallery--slide__image').select{|article| article["data-index"] == index.to_s return article}.first
  end

  def published_at
    session.first('.article-content-meta--posted-on').try(:text)
  end

  def slideshow_published_at
    session.first('time')[:datetime]
  end

  def listicle_slides
    session.all('.listicle--slide')
  end

  def listicle_slide_image(elem)
    elem.first('picture > source.picture-source-960')["srcset"].split(", ")[0]
  end

  def listicle_slide_title(elem)
    elem.first('.listicle--title').text
  end

  def listicle_slide_caption(elem)
    elem.first('.listicle--caption').text
  end

end
