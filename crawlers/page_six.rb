class PageSix < Zig::Crawler
  self.site_key = 'pagesix.com'
  self.site_root = 'http://pagesix.com/'
  self.max_attempts = 20

  protected

  def process_pages
    visit(site_root)

    (article_urls + gallery_urls).each do |url|
      visit(url)
      is_slideshow? ? crawl_slide_show(url) : crawl_article(url)
    end

  end

  def crawl_article(url)
    add_result do |result|
      result.title = article_title
      result.source = session.current_url
      result.path = article_image
      result.caption = article_description
      result.published_at = article_published_at
      result.people = pull_people(result.title + ' ' + result.caption)
    end
  end

  def is_slideshow?
    session.first('div.slides-wrapper') != nil
  end

  def article_urls
    session.all('div.home-page-section-stories-wrapper article.story-photo-box a').take(8).map { |link| link[:href] }.uniq
  end

  def article_image
    if img = session.first('div#featured-image-wrapper picture img')
      img[:srcset].to_s.split('&strip')[0]
    end
  end

  def article_title
    session.first('div.article-header h1 a').try(:text)
  end

  def article_published_at
    session.first('div.article-header p.byline-date').try(:text).split('|')[0]
  end

  def article_description
    session.first('div.entry-content p').try(:text)
  end

  def crawl_slide_show(url)
    gallery_slides.each_with_index do |slide,index|
      add_result do |result|
        result.title = title slide
        result.source = session.current_url
        result.path = image slide
        result.caption = caption slide
        result.published_at = published_at
        result.people = pull_people(result.title + ' ' + result.caption)
        result.series_item = index+1
        result.series_root = url
      end
    end
  end

  def gallery_urls
    session.all('div#home-page-post-photos article.story-photo-box a').take(8).map { |link| link[:href] }.uniq
  end

  def gallery_slides
    session.all('div.slides-wrapper div.slide', :visible => false).take(10)
  end

  def image(slide)
    slide.first('picture source', :visible => false)[:srcset].split('&strip=')[0]
  end

  def title(slide)
    slide.first('picture img', :visible => false)[:alt]
  end

  def caption(slide)
    slide.first('div.slide-caption', :visible => false).text(:all).split('Photo:')[0]
  end

  def published_at
    session.first('div.slideshow-date').try(:text)
  end
end
