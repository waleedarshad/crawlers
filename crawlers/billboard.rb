class Billboard < Zig::Crawler
  self.site_key = "billboard.com"
  self.site_root = "http://www.billboard.com/photos"
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      sleep 3

      index = 0

      slides.each_with_index do |slide, index|

        sleep 2

        if image(slide) && description(slide)
          add_result do |result|
            result.title = title(slide)
            result.source = session.current_url
            result.path = image(slide)[:src]
            result.caption = description(slide).first('p').try(:text)
            result.published_at = published_at
            result.series_root = url
            result.people = pull_people(result.title + " " + result.caption)
            result.series_item = index
          end
        end
      end
    end
  end

  def gallery_urls
    # session.all('div.field-item.even a').take(3).map { |gallery_link| gallery_link[:href] }.uniq #old implentation
    session.all('article.type-photo_gallery a').take(3).map { |gallery_link| gallery_link[:href] }.uniq
  end

  def slides
    session.all('div.swiper-slide')
  end

  def image(slide)
    slide.first('img.slide-image')
  end

  def title(slide)
    slide.first('div.slide-info__title').try(:text)
  end

  def description(slide)
    slide.first('div.slide-info__body')
  end

  def published_at
    session.first('div.gallery__published--date').try(:text)
  end
end
