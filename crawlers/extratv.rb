class Extratv < Zig::Crawler
  self.site_key     = 'extratv.com'
  self.site_root    = 'http://extratv.com/celebrity-news/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    post_urls.each do |url|
      visit(url)
      # sleep 1

      images.each_with_index do |image, index|
        if image
          add_result do |result|
            result.title    = title
            result.source   = session.current_url
            result.path     = image[:src]
            result.caption  = description
            result.published_at  = published_at
            result.people   = pull_people(result.title + ' ' + image[:alt])
            result.series_root = url if images.count > 1
            result.series_item = index+1 if images.count > 1
          end
        end
      end
    end
  end

  def post_urls
    session.all('div#main-wrap div.wbw-streamr-item a').take(8).map { |link| link[:href] }
  end

  def images
    images = []
    session.all('div.page-precontent img.img-responsive').map{|img| images << img}
    session.all('div.page-content img.img-responsive').map{|img| images << img}
    images
  end

  def title
    session.first('div.page-header-article h1').try(:text)
  end

  def description
    session.first('div.page-content-article p').try(:text)
  end

  def published_at
    session.first('span.publish-date time').try(:text)
  end
end