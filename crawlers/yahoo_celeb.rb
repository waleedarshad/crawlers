class YahooCeleb < Zig::Crawler
  self.site_key = 'yahoo.com'
  self.site_root = 'http://celebrity.yahoo.com/photos/'
  self.max_attempts = 35

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      sleep 1

      published = published_at

      if is_slideshow?
        start_slideshow

        5.times do |attempt|
          if slide_image != '' && slide_description != ''
            add_result do |result|
              result.title = slide_title
              result.source = session.current_url
              result.path = slide_image[:src]
              result.caption = slide_description
              result.published_at = published
              result.people = pull_people(result.title + ' ' + result.caption)
              result.series_root = session.current_url
              result.series_item = attempt+1
            end
          end

          click_next_slide
        end
      else
        if image
          add_result do |result|
            result.title    = title
            result.source   = session.current_url
            result.path     = image[:src]
            result.caption  = description
            result.published_at = published
            result.people = pull_people(result.title + ' ' + result.caption)
          end
        end
      end
    end
  end

  def gallery_urls
    session.all('div.js-stream-content > div > a').map { |link| link[:href] }.uniq.take(12)
  end

  def image
    session.first('figure.canvas-image img')
  end

  def start_slideshow
    session.first('svg[data-icon=gallery]', :visible => false).try(:click)
  end

  def is_slideshow?
    session.first('svg[data-icon=gallery]', :visible => false) != nil
  end

  def crawl_slideshow
  end

  def title
    if text = session.first('header.canvas-header h1').try(:text)
      text.to_s.gsub("Related Search Result", "").strip
    end
  end

  def description
    session.first('div.canvas-body p').try(:text)
  end

  def slide_image
    session.first('img.slideshow-image')
  end

  def slide_title
    if text = session.first('h1.slideshow-headline').try(:text)
      text.to_s.gsub("Related Search Result", "").strip
    end
  end

  def slide_description
    session.first('div.slideshow-description').try(:text)
  end

  def published_at
    session.first('div.date').try(:text)
  end

  def click_next_slide
    session.first('svg[data-icon=CoreArrowRight]').try(:click)

    sleep 1
  end
end
