require 'simple-rss'
require 'open-uri'
require 'htmlentities'
class EOnline < Zig::Crawler
  self.site_key = "eonline.com"
  self.site_root = "http://www.eonline.com/photos"
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      image_list.each_with_index do |image, index|
        add_result do |result|
          result.title = title(index)
          result.source = session.current_url
          result.path = image[:src]
          result.caption = description
          result.published_at = "#{Date.today}"
          result.people = pull_people(result.title + " " + result.caption)
          result.series_root = url
          result.series_item = index
        end
        click_next_slide
      end
    end
  end

  def gallery_urls
    session.all('ul#gallery-list a').take(10).map { |link| link[:href] }
  end

  def description
    session.find('meta[name="description"]', :visible => false)[:content]
  end

  def image_list
    session.all('div.slide-reel div.slide.zoomable div.imgWrapper img').take(8)
  end

  def title(index)
    session.all('div.slide-selector-fixed-panel div.selector-item a img')[index][:title]
  end

  def click_next_slide
    session.first('div#hh-carousel a.arrow-link-right[rel=next]').trigger('click')
  end
end