class MediaTakeout < Zig::Crawler
  self.site_key = "mediatakeout.com"
  self.site_root = "http://www.mediatakeout.com"
  self.max_attempts = 15

  protected

  def process_pages
    visit(site_root)


    article_urls.each do |url|
      visit(url)

      images.each_with_index do |image, index|
        add_result do |result|
          result.title = title
          result.source = session.current_url
          result.path = image[:src]
          result.caption = caption
          result.published_at = published_at
          result.people = people
          result.series_root = url
          result.series_item = index
        end
      end
    end

  end

  def article_urls
    session.all('a.article').collect { |article| article[:href] }.take(8)
  end

  def title
    session.first('a.article').text
  end

  def published_at
    session.first('time')['datetime']
  end

  def caption
    session.first('div.article > p').text
  end

  def people
    pull_people(caption)
  end

  def images
    session.all('img.center.scale-with-grid')
  end

end