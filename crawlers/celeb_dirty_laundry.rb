class CelebDirtyLaundry < Zig::Crawler
  self.site_key     = 'celebdirtylaundry.com'
  self.site_root    = 'http://www.celebdirtylaundry.com/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)
    story_links.each do |link|

      begin
        visit(link)
        sleep 3

          if images.length == 1
            add_result do |result|
              result.title        = title
              result.source       = session.current_url
              result.path         = image
              result.caption      = caption
              result.published_at = published_at
              result.people       = pull_people(result.title + ' ' + result.caption)
            end
          else
            # EXTRA GALLERY ADDITION
            # if gallery_exist?
            #
            #   gallery_first_image.click
            #   sleep 3
            #
            #   while next_slide_exist?
            #     index = 0
            #     add_result do |result|
            #
            #       result.title        = title
            #       result.source       = session.current_url
            #       result.path         = slide_image
            #       result.caption      = caption
            #       result.published_at = published_at
            #       result.people       = pull_people(result.title + ' ' + result.caption)
            #       result.series_root  = session.current_url
            #       result.series_item  = index
            #     end
            #     index = index + 1
            #     next_slide.click
            #     sleep 3
            #   end

            # else
              images.each_with_index do |img,index|
                add_result do |result|
                  result.title        = title
                  result.source       = session.current_url
                  result.path         = !img[:src].blank? ? img[:src] : img[:srcset]
                  result.caption      = caption
                  result.published_at = published_at
                  result.people       = pull_people(result.title + ' ' + result.caption)
                  result.series_root  = session.current_url
                  result.series_item  = index
                end
              end
            end

      rescue => e
        print("==================#{e}===============> exectipn")
      end

    end
end

  def slide_image
    img  = session.first(".attach-img img")
    img = !img[:src].blank? ? img[:src] : img[:srcset]
    img
  end
  def next_slide_exist?

    session.all(".entry .attach-link a").select{|a| return true if a.try(:text) == "Next"}
  end

  def next_slide

      session.all(".entry .attach-link a").select{|a| return a if a.try(:text) == "Next"}
  end

  def story_links
    session.all('.entry-title a').collect{|a| a['href']}.compact
  end

  def title
    session.first('.entry-title a').try(:text)
  end

  def caption
    title
  end

  def images
    img = session.all('.entry img')
  end

  def image
    img = session.first('.entry img')
    img = !img[:src].blank? ? img[:src] : img[:srcset]
    img
  end

  def gallery_exist?
    session.first(".gallery-1").present?
  end

  def gallery_first_image
      session.first(".gallery-1").first("img")
  end

  def published_at
    session.first(".post-meta").try(:text).split("|")[0].split("ON")[1]
  end

end
