require 'open-uri'
require 'rss'
class UsWeekly < Zig::Crawler
  self.site_key     = 'usmagazine.com'
  self.site_root    = 'http://usmagazine.com'
  self.max_attempts = 40



  protected

  def process_pages

    visit(galleries_root)

    gallery_urls.each do |url|
      page = 1
      while page < 10
        visit("#{url}?page=#{page}")
        page = page + 1
        sleep 3
        index = 0
        slides.each_with_index do |slide, index|
          if image(slide) && description(slide)

            next if description(slide) =~ /sign up/i

            add_result do |result|
              result.title = title(slide)
              result.source = session.current_url
              result.path = image(slide)[:src]
              result.caption = description(slide).first('p').try(:text)
              result.published_at = published_at
              result.series_root = url
              result.people = pull_people(result.title + " " + result.caption)
              result.series_item = index
            end
          end
        end
        session.driver.reset!
      end
    end

    # RSS Feed for news articles
    rss_content = ""
    open(rss_news_root) do |f|
       rss_content = f.read
    end

    # Parse the feed, dumping its contents to rss
    rss = RSS::Parser.parse(rss_content, false)

    rss.items.each do |item|
      rss_title = item.title
      published = item.pubDate

      visit(item.link)
      sleep 3
      if inline_images.count > 1
        inline_images.each_with_index do |elem, index|
          add_result do |result|
            result.title        = caption_for(elem)
            result.source       = session.current_url
            result.path         = image_path_for(elem)
            result.caption      = caption_for(elem)
            result.published_at = published
            result.people       = pull_people([rss_title, caption_for(elem)].join(" "))
            result.series_root  = item.link
            result.series_item  = index + 1
          end


        end


      elsif inline_images.count == 1

        add_result do |result|
          result.title        = rss_title
          result.source       = session.current_url
          result.path         = image_path_for(inline_images.first)
          result.caption      = caption_for(inline_images.first)
          result.published_at = published
          result.people       = pull_people([rss_title, caption_for(inline_images.first)].join(" "))
        end
      end
      session.driver.reset!

    end

  end

  def slides
    session.all('div.gallery-body-content-slide-image-container')
  end

  def image(slide)
    slide.first('div.gallery-body-content-slide-image img')
  end

  def title(slide)
    slide.first('div.gallery-slide-description h3').try(:text)
  end

  def description(slide)
    slide.first('div.gallery-slide-description')
  end

  def published_at
    Time.now
  end

  def gallery_urls
    session.all('a').select do
      |link| link.text.start_with?('VIEW FULL GALLERY')
    end.map{|link| link[:href]}.uniq.take(12)
  end


  def galleries_root
    "http://www.usmagazine.com/celebrity-pictures"
  end


  def hot_pics_root
    "http://www.usmagazine.com/hot-pics"
  end

  def rss_news_root
    "http://www.usmagazine.com/celebrity_news/rss"
  end

  def inline_images
    article_body_images.to_a | article_image_div.to_a
  end

  def article_body_images
    session.find('.article-body').all('.article-body-content-main-photo') rescue []
  end

  def article_image_div
    session.all('div.inset-image.inset-full') rescue []
  end


  def caption_for(elem)
    if elem[:class] =~ /article-body-content-main-photo/
      elem.first('.article-body-content-main-photo-caption').try(:text)
    elsif elem[:class] =~ /inset-image/
      elem.first('span.caption').try(:text)
    end
  end

  def image_path_for(elem)
      elem.first('img')[:src]
  end

end
