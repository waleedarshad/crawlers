class Celebuzz < Zig::Crawler
  self.site_key     = 'celebuzz.com'
  self.site_root    = 'http://www.celebuzz.com/photos/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    post_urls.each do |url|
      visit(url)
      sleep 1
      visit(gallery_link)

      image_links.times do |index|
        if image
          add_result do |result|
            result.title    = title
            result.source   = session.current_url
            result.path     = image[:src]
            result.caption  = description
            result.published_at  = published_at
            result.people   = pull_people(result.title + ' ' + result.caption)
            result.series_root = url
            result.series_item = index+1
          end
        end
        next_slide
        sleep 2
      end
    end
  end

  def post_urls
    session.all('div.feed div.post div.image-holder a').take(5).map { |link| link[:href] }
  end

  def gallery_link
    session.first('div.gallery-view-gallery-link a')[:href]
  end

  def image_links
    session.all('div.slick-track div.gallery-slide a', :visible => false).count
  end

  def image
    session.first('div.slick-track div.gallery-slide.slick-active a img')
  end

  def title
    session.first('div.info-holder h2').try(:text)
  end

  def description
    session.first('div.info-holder div.content').try(:text)
  end

  def published_at
    session.first('span.date').try(:text).split('-')[0].gsub('|', '')
  end

  def next_slide
    if btn = session.first('div.gallery-arrow.next')
      btn.trigger(:click)
    end
  end
end
