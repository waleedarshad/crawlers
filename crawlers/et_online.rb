class EtOnline < Zig::Crawler
  self.site_key = 'etonline.com'
  self.site_root = 'http://www.etonline.com/gallery/'
  self.max_attempts = 20

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)
      total_length = image_cells.length
      image_cells.each_with_index do |cell, index|
        
        add_result do |result|
          result.title = title(cell)
          result.source = url+"##{total_length}"
          result.path = image(cell)
          result.caption = desciption(cell)
          result.published_at = "#{Date.today}"
          result.series_root = url 
          result.series_item = index+1
          result.people = pull_people(result.title + ' ' + result.caption)
        end
        total_length = total_length -1
      end
    end
  end

  def desciption(cell)
    paragraph = ''
    caption(cell).all('p').each do |pgraph|
      paragraph += pgraph.try(:text)+"\n"
    end
    paragraph
  end

  def gallery_urls
    session.all(:xpath, '//a[contains(@href,"gallery/")]').take(8).map do |link|
      link[:href].gsub('http://www.etonline.com', 'http://www.etonline.com/slideshow')
    end.uniq
  end

  def image(cell)
    cell.first('div.fill picture img')["data-src"]
  end

  def title(cell)
    cell.first('div#photoInfo div.title_wrapper h3').text
  end

  def caption(cell)
    cell.first('div#photoInfo div.caption')
  end

  def image_cells
    session.all('div#gallery-container div.cell')
  end

  def published_at(cell)
    cell.find('div.date').nil? ? "#{Date.today}" : cell.find('div.date').text
  end
end
