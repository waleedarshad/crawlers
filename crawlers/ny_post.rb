class NyPost < Zig::Crawler
  self.site_key     = 'nypost.com'
    self.site_root    = 'http://nypost.com/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)
    sleep 3

    session.first("#sections").click

    story_links.each do |link|
      begin
        visit(link)
          combine_links.each_with_index do |link, index|
            session.driver.restart if session.driver.respond_to? :restart
            visit(link)
            if image
              add_result do |result|
                result.title        = title
                result.source       = session.current_url
                result.path         = image
                result.caption      = caption
                result.people       = pull_people(result.title + ' ' + result.caption)
                result.published_at = published_at
              end
            end
          end
        rescue => e
          print("==================#{e}===============> exception")
          session.driver.restart if session.driver.respond_to? :restart

        end
    end
end


  def story_links
    links = []
    session.all('#section-nav-wrapper li a').collect do |a|
      if a.try(:text).downcase.start_with?("entertainment","fashion","sports", "page six")
        links.push(a[:href])
      end

    end

    links
  end

  def section_top_stories
    session.all(".section-top-stories-wrapper article  a").collect{|a| a['href']}.compact.uniq rescue []
  end

  def link(top_story)
    top_story.first("a")
  end

  def article_title
    session.first(".article-header a").try(:text)
  end

  def article_image
    img = session.first(".article-header").first("picture").first("img")
    img = !img[:src].blank? ? img[:src] : img[:srcset]
    img
  end

  def article_description
    paragraph = ""

    session.all('.entry-content p').each do |node|
      paragraph += node.try(:text)+"\n"
    end
    paragraph

  end

  def article_published_at
    session.first(".byline-date").try(:text).split("|")[0]
  end

  def latest_articles
    session.all("#primary header a").collect{|a| a['href'] if a['href']}.compact.uniq rescue []
  end

  def combine_links
    section_top_stories + latest_articles + page_six_story_link
  end

  def title
    session.first('.article-header h1 a').try(:text)
  end

  def caption
     session.first(".wp-caption-text").try(:text) || title
  end

  def image
    img = session.first('#featured-image-wrapper img')
    if img
      img = !img[:src].blank? ? img[:src] : img[:srcset]
      img
    else
      nil
    end
  end

  def page_six_story_link
    session.all(".home-page-section-stories-wrapper a").collect{|a| a['href'] if a['href']}.compact.uniq rescue []
  end

  def published_at
    session.first(".byline-date").try(:text).split("|")[0]
  end

end
