class Cnn < Zig::Crawler
  self.site_key     = "cnn.com"
  self.site_root    = "http://www.cnn.com/showbiz"
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      sleep 1

      gallery_items.each_with_index do |item,index|

        if item['data-slidename'] && caption(item)
          add_result do |result|
            result.title        = item['data-slidename']
            result.source       = session.current_url
            result.path         = image(item)
            result.caption      = caption(item)
            result.published_at = published_at
            result.people       = pull_people(result.title + ' ' + result.caption)
            result.series_root  = url
            result.series_item  = index+1
          end
        end

      end
    end
  end

  def gallery_urls
    session.all('article.cd.cd--card.cd--gallery a', :visible => false).take(8).map { |gallery_link| gallery_link[:href] }.uniq
  end

  def gallery_items
    session.all('div[data-analytics=_body_image]', :visible => false)
  end

  def image(item)
    item.first('img.media__image', :visible => false)['data-src-medium']
  end

  def caption(item)
    item.first('div.media__caption.el__gallery_image-title span.el__storyelement__gray', :visible => false).text(:all)
  end

  def published_at
    session.find('p.update-time').text.gsub('Updated', '')
  end
end
