class Ew < Zig::Crawler
  self.site_key = 'ew.com'
  self.site_root = 'http://www.ew.com/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    top_links.each do |url|
      visit(url)

      index = 0
      if image && description
        add_result do |result|
          result.title = title
          result.source = session.current_url
          result.path = image[:src]
          result.caption = description
          result.people = pull_people(result.title + ' ' + result.caption)
          result.published_at = published_at
          result.series_root = url
          result.series_item = index
        end
      end
      # index += 1
    end
  end

  def top_links
    session.all('section.block.block-top_stories li a').map { |link| link[:href] }.uniq
  end

  def number_of_slides
    session.find('span#photoGalleryCount').text.split(' of ')[1].to_i
  end

  def image
    session.first('article.node-article div.content div.article-main-image img')
  end

  def title
    session.first('article.node-article header.node-header h1').try(:text)
  end

  def description
    session.first('article.node-article header.node-header p.deck').try(:text)
  end

  def published_at
    session.first('article.node-article div.content p.submitted').try(:text)
  end
end