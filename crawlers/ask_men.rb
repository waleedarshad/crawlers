class AskMan < Zig::Crawler
  self.site_key     = "askmen.com"
  self.site_root    = "http://www.askmen.com/galleries/"
  self.max_attempts = 15

  protected

  def process_pages
    visit(site_root)

    sleep 2

    gallery_urls.each do |url|
      visit(url)

      sleep 2

      in_gallery_of_unknown_length do |attempt|
        if image && description
          src = image[:src]

          if src =~ /picture-(\d)-(\d*)\.jpg/
            add_result do |result|
              result.title        = title.text.to_s.strip.gsub(': Photos', '').strip
              result.source       = session.current_url
              result.path         = src
              result.caption      = description
              result.people       = pull_people(result.title + " " + result.caption)
              result.series_root  = url
              result.published_at = published_at
              result.series_item  = attempt
            end
          end
        end
        click_next_slide
      end
    end
  end

  def gallery_urls
     session.all("div.articleTile.photoGallery.bigBlock div.square.odd a").take(15).map{|gallery_link| gallery_link[:href]}.uniq
  end

  def image
    session.first('.fotorama__active img')
  end

  def title
    session.first('.hLargeTitle')
    end

  def description
    session.first('div.theArticle div.caption').try(:text)
  end

  def published_at
    session.first('div.theArticle div.authorName time').try(:datetime)
  end

  def click_next_slide
    session.first('.fotorama__arr--next').try(:trigger, :click)
    sleep 4
  end
end
