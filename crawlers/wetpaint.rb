require 'rss'
class Wetpaint < Zig::Crawler
  self.site_key     = "wetpaint.com"
  self.site_root    = "http://www.wetpaint.com"
  self.max_attempts = 15

  protected

  def process_pages
    articles.each do |article|
      visit(article.url)

      if photos.count > 1
        photos.count.times do |n|
          desc  = article.description || article_description
          add_result do |result|
            result.title        = article.title
            result.source       = article.url
            result.path         = photos[n].first('img')[:src]
            result.caption      = desc
            result.published_at = article.published_at
            result.people       = pull_people(article.title + " " + desc)
            result.series_root  = article.url
            result.series_item  = n
          end
        end
      elsif image
        desc  = article.description ||  caption || article_description
        add_result do |result|
          result.title        = article.title
          result.source       = article.url
          result.path         = image
          result.caption      = desc
          result.published_at = article.published_at
          result.people       = pull_people(article.title + " " + desc)
        end
      end
    end

  end

  def article_description
    if session.first(".wysiwyg-block")
      paragraph = ''
      session.first(".wysiwyg-block").all('p').each{|pgraph| paragraph += pgraph.try(:text)+"\n" }
      paragraph
    else
      return ''
    end
  end

  def caption
    caption = session.first(".wp-content-main-credit")
    if caption
      caption.try(:text)
    end
  end
  def articles
    rss = RSS::Parser.parse('http://www.wetpaint.com/feed/', false)
    rss.channel.items.map{|item| WetpaintArticle.new_from_rss(item)}
  end

  def people
    session.first('div.wp-app-home-author > p').all('a > span').collect{|span| span.text}.to_sentence
  end

  def image(node=session)
    node.first('div.wp-content-main-img > div.wp-content-img > img')[:src] rescue nil
  end

  def photos
    session.all('div.wp-content-img')
  end

end

class WetpaintArticle

  attr_accessor :url, :title, :published_at, :description

  def self.new_from_rss(rss_item)

    instance = self.new.tap do |article|
      article.title        = rss_item.title
      article.url          = rss_item.guid.content
      article.published_at = rss_item.pubDate
      article.description  = rss_item.description[/(?<=<\/div>\s).*\Z/].try(:strip)
    end


  end

end
