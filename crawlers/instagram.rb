class Instagram < Zig::Crawler
  self.site_key   = 'instagram.com'
  self.site_root  = 'https://instagram.com'

  attr_reader :item

  protected

  def load_items
    visit(site_root)
    2.times do
      session.execute_script "window.scrollBy(0,10000)"
      sleep 3
    end


    post_urls.each do |url|

      visit(url)
      sleep 2

      next unless image_url(article)
      add_result do |result|
        result.title        = username(article)
        result.caption      = caption(article)
        result.source       = session.current_url
        result.path         = image_url(article)[:src]
        result.people       = username(article)
        result.published_at = published_at(article)[:datetime]
      end
    end
  end

  def process_pages

    cookie = session.driver.browser.cookies["sessionid"]

    if  cookie.blank? or (cookie.present? and cookie.expires < Date.today)
      perform_login
    end
    load_items
  end

  def perform_login
      visit(signin_url)

      session.find_field('username').set(ENV['INSTAGRAM_USERNAME'])
      session.find_field('password').set(ENV['INSTAGRAM_PASSWORD'])
      session.find('button').click
      sleep 3

  end

  def signin_url
    'https://instagram.com/accounts/login/'
  end

  def post_urls
    post_urls = []
    session.all("._ljyfo").map do |item|
      post_urls.push(item[:href])
    end
    post_urls
  end

  def image_url(article)
    article.all('img')[1]
  end

  def username(article)
    article.first('._f95g7 a').text
  end

  def caption(article)
    article.first('li h1 span').text rescue nil
  end

  def published_at(article)
    article.find('time')
  end

  def article
    session.find("article")
  end
end
