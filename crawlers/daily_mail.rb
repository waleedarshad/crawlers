class DailyMail < Zig::Crawler
  self.site_key     = 'dailymail.co.uk'
  self.site_root    = 'http://www.dailymail.co.uk/tvshowbiz/pictures/index.html'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      image_links.each do |link|
        if image(link)
          add_result do |result|
            result.title    = image(link)[:title]
            result.source   = session.current_url
            result.path     = link[:href]
            result.caption  = image(link)[:alt]
            result.published_at  = published_at(link)
            result.people   = pull_people(result.title + " " + result.caption)
          end
        end
      end
    end
  end

  def gallery_urls
    session.all('.gallery-thin a[rel=nofollow]').take(12).map do |link|
      link[:href]
    end
  end

  def image_links
    session.all('div.scroller a.pagelinks.bdrw')
  end

  def image(link)
    link.first('img')
  end

  def published_at(link)
    rel = link[:rel]
    date = rel.split('/')
    date[6].gsub('_','/') + '/' + date[5]
  end
end
