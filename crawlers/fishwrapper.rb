class Fishwrapper < Zig::Crawler
  self.site_key = "fishwrapper.com"
  self.site_root = "http://www.fishwrapper.com"
  self.max_attempts = 15

  protected

  def process_pages
    visit(site_root)

    article_urls.each do |url|

      visit(url)
      sleep 1

      add_result do |result|
        result.title = title
        result.source = session.current_url
        result.path = image
        result.caption = caption
        result.published_at = published_at
        result.people = people
        if has_gallery?
          result.series_root = session.current_url
          result.series_item = 0
        end
      end

      if has_gallery?
        visit(gallery_links[0][:href])

        current, total = session.first('div.image-count').text.split("/")

        total.to_i.times do |n|
          current, total = session.first('div.image-count').text.split("/")
          begin
            add_result do |result|
              result.title = session.first('.gallery-title').text
              result.source = session.current_url
              result.path = session.first('.image-wrapper > .image > .tmz-image > img')[:src]
              result.caption = gallery_caption
              result.published_at = Time.now
              result.people = gallery_caption
              result.series_root = url
              result.series_item = current.to_i
            end
            session.first('div.next').click unless n == (total.to_i - 1)
            sleep 1
          end
        end
      end

    end
  end

  def article_urls
    session.all("div.items > .post.item").collect { |div| div.first('div.title > h2 > a')[:href] }
  end

  def title
    session.first('div.page-contents-wrap > div.title').text
  end

  def published_at
    session.first('span.publish-date').text[/\A.*(?=, by)/]
  end

  def has_gallery?
    !gallery_links.empty?
  end

  def gallery_links
    session.all('.contents.clearfix > a').select { |a| link_to_slideshow?(URI(a[:href])) == true }
  end

  def link_to_slideshow?(uri)
    uri.host == 'www.fishwrapper.com' && uri.path.split('/')[1] == 'photos'

  end

  def caption
    session.first('.contents.clearfix').text
  end

  def gallery_caption
    session.first(".image-caption").text
  end

  def people
    session.first('.filed-under').text
  end

  def image(node = session)
    session.first('.contents.clearfix img')[:src]
  end

end
