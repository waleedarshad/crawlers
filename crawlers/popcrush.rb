class Popcrush < Zig::Crawler
  self.site_key     = 'popcrush.com'
  self.site_root    = 'http://popcrush.com'
  self.max_attempts = 5

  protected

  def process_pages
    visit(site_root)

    article_urls.each do |url, title|

      visit(url)

      next if no_article_images

      if article_images.length > 1
        article_images.each_with_index do |image_node, index|
          add_result do |result|
            result.title        = title
            result.source       = session.current_url
            result.path         = image_node[:src]
            result.caption      = article_image_caption(image_node)
            result.published_at = published_at
            result.people       = pull_people(main_post.first('p').text)
            result.series_root  = url
            result.series_item  = index
          end
        end
      else
        image = article_images[0]
        add_result do |result|
          result.title        = title
          result.source       = url
          result.path         = image[:src]
          result.caption      = article_image_caption(image)
          result.published_at = published_at
          result.people       = pull_people(main_post.first('p').text)
        end
      end
    end


    visit(site_root + "/category/galleries/")

    gallery_urls.each do |url, title|
      visit(url)
      index = 0

      series_title = session.first('article > h1').text.titleize

      session.first('img.tsm-photogallery-item-image').click

      #TODO: making this first instead of all grabs the first slideshow on the page. There are sometimes multiple slideshows,
      # but without determining a better way to get a title for the secondary show, we're leaving it alone for now.
      slide_count = number_of_slides
      until index > slide_count do
        if image && description && !found_slide_ad_unit?
          add_result do |result|
            result.title        = series_title
            result.source       = session.current_url
            result.path         = image[:src]
            result.caption      = description_copy_else_title
            result.published_at = published_at
            result.people       = description.try(:text)
            result.series_root  = url
            result.series_item  = index
          end
          index += 1
        end

        click_next_slide

      end
    end
  end

  def article_urls
    hsh = {}
    session.find('div#content_main').all('article').each do |art|
      link = art.first('a')
      hsh[link[:href]] = link.native.all_text #the Capybara::Poltergeist::Node native instance allows us to ignore CSS text transforms
    end
    hsh
  end

  def gallery_urls
    session.all('section.blogroll.row-standard article figure a').map { |link| link[:href] }.uniq.take(3)
  end

  def main_post
    session.find('div#content > div#content_main > section.single > article > div.the_content')
  end

  def article_images
    main_post.all('figure img').select{|img| img[:class] != "tsm-photogallery-item-image"}
  end

  def article_image_caption(node)
    node.parent.first('figcaption').text
    rescue
    main_post.first('p').text
  end


  def no_article_images
    article_images.empty?
  end

  def number_of_slides
    session.first('span.tsm-photogallery-total').text.to_i
  end

  def image
    session.first('img.tsm-photogallery-item-image')
  end

  def description
    session.first('div.tsm-photogallery-copy')
  end

  def found_slide_ad_unit?
    !!session.first('.tsm-photogallery-card-taboola')
  end

  def description_header
    description.first('h2').text
  end

  def description_paragraph
    # ooooooh presence! nice
    description.first('p').text.presence
  end

  def description_copy_else_title
    description_header + ": " + description_paragraph
    rescue
    description_header
  end

  def published_at
    session.find('.the_date').text
  end

  def click_next_slide
    session.first('a.icon-right-open.next-btn').click

    sleep 1
  end
end
