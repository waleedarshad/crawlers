class NyDailyNews < Zig::Crawler
  self.site_key     = 'nydailynews.com'
  self.site_root    = 'http://www.nydailynews.com/photos/entertainment'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    article_urls.each do |url|
      visit(url)

      8.times do |index|  # No element found for number of slides count so we have made it static to just parse and save only first 8 slides
        sleep 1
        click_next_slide

        if image && description
          add_result do |result|
            result.title        = title
            result.source       = session.current_url
            result.path         = image[:src]
            result.caption      = description
            result.people       = pull_people(result.title + ' ' + result.caption)
            result.published_at = published_at
            result.series_root  = url
            result.series_item  = index+1
          end
        end
      end
    end

  end

  def article_urls
    session.all('div#photo-wall div.teaser a').map {|link| link[:href] }.uniq.take(10)
  end

  def description
    session.first('meta[property="og:description"]', :visible => false)[:content]
  end

  def published_at
    session.first('meta[name="parsely-pub-date"]', :visible => false)[:content]
  end

  def image
    session.find('img#rgs-img', :visible => false)
  end

  def click_next_slide
    node = session.find('a#rgc-to-s-2', :visible => false)
    if node
      node.trigger('click')
      return
    end
    session.find('nav#rgs-nav a#rgs-next', :visible => false).trigger('click')
  end

  def title
    session.first('meta[property="og:title"]', :visible => false)[:content]
  end
end
