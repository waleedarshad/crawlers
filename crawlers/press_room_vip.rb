class PressRoomVip < Zig::Crawler
  self.site_key     = 'pressroomvip.com'
  self.site_root    = 'http://pressroomvip.com/category/entertainment/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    post_urls.each do |url|
      visit(url)
      # sleep 1

      slides.each_with_index do |slide, index|
        if image(slide)
          add_result do |result|
            result.title    = title(slide)
            result.source   = session.current_url
            result.path     = image(slide)[:src]
            result.caption  = description(slide)
            result.published_at  = published_at
            result.people   = pull_people(result.title + ' ' + result.caption)
            result.series_root = url if slides.count > 1
            result.series_item = index+1 if slides.count > 1
          end
        end
      end
    end
  end

  def post_urls
    session.all('article.gallery a').take(8).map { |link| link[:href] + '2/?ipp=3' }
  end

  def slides
    session.all('article.gallery div.gallery-standard-slide')
  end

  def title(slide)
    slide.first('h4.slide_title').try(:text)
  end

  def image(slide)
    slide.first('div.image-or-embed img')
  end

  def description(slide)
    slide.all('p').last.try(:text)
  end

  def published_at
    session.first('article.gallery header p.info-meta').try(:text).split('AT')[0]
  end
end