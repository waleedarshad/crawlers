class Vulture < Zig::Crawler
  self.site_key     = 'vulture.com'
  self.site_root    = 'http://www.vulture.com/news/slideshow/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      sleep 1

      series_pub_date = published_at
      details = slide_details

      image_slides.each_with_index do |image,index|
        if image
          add_result do |result|
            result.title        = slide_title(details[index])
            result.source       = session.current_url
            result.path         = image[:src]                # maybe a better way to deal with protocol relative src?
            result.caption      = slide_caption(details[index])
            result.published_at = series_pub_date
            result.people       = pull_people(result.title + ' ' + result.caption)
            result.series_root  = url
            result.series_item  = index + 1
          end
        end
      end

      session.driver.reset!
    end

  end

  def gallery_urls
    session.all('a.thumb.infographic').map { |link| link[:href].gsub('.html','/slideshow/') }.uniq.take(6)
  end

  def image_slides
    session.all('figure.img img', :visible => false)
  end

  def slide_details
    session.all('aside.slide-sidebar', :visible => false)
  end

  def slide_title(aside)
    if title = aside.first('h2.slide-title', :visible => false)
      title.text(:all)
    end
  end

  def slide_caption(aside)
    if caption = aside.first('div.slide-description p', :visible => false)
      caption.text(:all)
    end
  end

  def published_at
    # binding.pry
    session.first('time')['datetime']
  end
end
