class FoxNews < Zig::Crawler
  self.site_key     = 'foxnews.com'
  self.site_root    = 'http://www.foxnews.com/entertainment/slideshows/index.html'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      close_annoying_banner

      slideshow_metadata.each_with_index do |meta, index|
        add_result do |result|
          result.title        = property_for(meta, :name)
          result.source       = url_for(meta)
          result.path         = property_for(meta, :image)
          result.caption      = property_for(meta, :description)
          result.series_root  = url
          result.series_item  = index
        end
      end

    end
  end

  def gallery_urls
    session.all('li.slideshow-ct article a').map do |link|
      link[:href]
    end.uniq.take(5)
  end

  def slideshow_metadata
    session.all('div[itemprop="isPartOf"]', visible: false)
  end

  def property_for(meta, type)
    element = meta.find("[itemprop='#{type}']", visible: false)

    case type
    when :url, :name, :description
      element.native.all_text
    when :image
      element['data-src']
    end
  end

  def url_for(meta)
    url = property_for(meta, :url)

    session.current_url.gsub(%r{#/slide/.*$}, "#/slide/#{url}")
  end

  def close_annoying_banner
    session.find('#close-button').trigger('click') if session.has_css?('#close-button')
  end
end
