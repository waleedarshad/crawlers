class Radar < Zig::Crawler
  self.site_key     = "radaronline.com"
  self.site_root    = "http://www.radaronline.com"
  self.max_attempts = 15

  protected

  def process_pages
    visit(site_root)

    article_urls.each do |url|

      visit(url)

      if is_gallery?

        @caption = caption
        @index = 0

        slide_holders.each do |div|
          if image(div)
            add_result do |result|
              result.title        = title
              result.source       = session.current_url
              result.path         = image(div)
              result.caption      = gallery_caption(div)
              result.published_at = Time.now
              result.people       = people
              result.series_root  = session.current_url
              result.series_item  = @index
            end
            @index += 1
          end

          session.first('a.owl-next').click

        end

      else

        add_result do |result|
          result.title        = title
          result.source       = session.current_url
          result.path         = image
          result.caption      = caption
          result.published_at = published_at
          result.people       = people
        end
      end
    end

  end

  def article_urls
    session.all("div#content-columns > article")
      .collect{|article| article.first('a')[:href]}
      .reject{|url| URI(url).host != 'radaronline.com'}
  end

  def title
    session.first('.entry-title').text
  end

  def published_at
    session.first('span.single-posted-on').text.gsub("Posted on ", "")
  end

  def is_gallery?
    !session.first('#ami-gallery').nil?
  end

  def caption
    session.first('#ami-post-content').text
  end

  def gallery_caption(div)
    div.first(".ami-gallery-entry").text.empty? ? @caption : div.first(".ami-gallery-entry").text
  end

  def slide_holders
    session.all('.owl-item')
  end

  def people
    tags = session.first('div.tags')
    tags ? tags.all('a').collect{|a| a.text}.to_sentence : ''
  end

  def image(node = session)
    img_tag = node.first('.entry img')
    img_tag.nil? ? nil : img_tag[:src]
  end

end
