
class Tmz < Zig::Crawler
  self.site_key     = 'tmz.com'
  self.site_root    = 'http://www.tmz.com'
  self.max_attempts = 10

  protected

  def process_pages

    visit(site_root)

    article_urls.each do |url|
      begin
        visit(url)
        
        if has_photos?
          
          if multiple_photos?
            
            unless inline_photos.empty?
              
              inline_photos.each_with_index do |img, index|
                
                add_result do |result|
                  result.title        = article_title
                  result.source       = session.current_url
                  result.path         = img[:src]
                  result.caption      = caption
                  result.people       = pull_people(article_title + " " + caption)
                  result.series_root  = session.current_url
                  result.series_item  = index+1
                  result.published_at = published_at
                end
              end
            end
            
            unless !gallery_link
              
              series_root = session.current_url
              main_article_title = article_title
              main_description = description
              published = published_at
              
              visit(gallery_link[:href])

              number_of_slides.times do
                
                if image
                  add_result do |result|
                    result.title        = gallery_caption
                    result.source       = session.current_url
                    result.path         = image
                    result.caption      = gallery_caption
                    result.people       = pull_people(main_article_title + ' ' + gallery_caption + ' ' + main_description)
                    result.series_root  = series_root
                    result.series_item  = @index
                    result.published_at = published
                  end
                  @index += 1
                end
                click_next_slide
              end
            end
          else
            
            add_result do |result|
              result.title        = article_title
              result.source       = session.current_url
              result.path         = inline_photos[0][:src]
              result.caption      = caption
              result.people       = pull_people(article_title + ' ' + caption)
              result.published_at = published_at
            end
          end
        end
        session.driver.reset!
        session.driver.restart if session.driver.respond_to? :restart
      rescue Exception => e  
        
      end
    end
  end

  def article_urls
    session.find('div#main-content').all('article.news').collect{|art| art.first('a.headline')[:href]}
  end

  def has_photos?

    !!(!inline_photos.empty? || gallery_link)
  end

  def multiple_photos?

    !!(inline_photos.count > 1 || gallery_link)
  end

  def gallery_link

    session.find('.article-content a.lightbox-link') rescue nil
  end

  def inline_photos

    session.all('article.news.single div.all-post-body > p > img')
  end

  def article_title
    session.find('.post-title-breadcrumb').text
  end

  def caption
    session.find('.article-content').all('p').collect{|p| p.text}.join(" ") || ""
  end

  def description
    session.first('meta[name="description"]', :visible => false)[:content]
  end

  def published_at
    if session.first('meta[itemprop="datePublished"]', :visible => false)
      session.first('meta[itemprop="datePublished"]', :visible => false)[:content]
    else

regex = /\d{1,2}\/\d{1,2}\/\d{4}/
      session.first(".article-posted-date").try(:text)[regex]
    end

  end

  def number_of_slides
    session.find('span.gallery-total').text.to_i
  end

  def image
    session.first(".gallery-slide.gallery-image-slide.slick-slide.slick-current.slick-active")["data-ggsrc"] rescue nil
  end

  def gallery_caption
    session.first('div.gallery-title').try(:text)
  end

  def click_next_slide
    session.find('div.gallery-arrow-next').click
  end
end
