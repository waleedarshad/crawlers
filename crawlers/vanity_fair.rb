require 'open-uri'
require 'rss'
   
class VanityFair < Zig::Crawler
  self.site_key     = 'vanityfair.com'
  self.site_root = 'http://vanityfair.com/rss'
  self.max_attempts = 100

  protected


  def process_pages

    rss_content = ""
    open(site_root) do |f|
       rss_content = f.read
    end

      # Parse the feed, dumping its contents to rss
    rss = RSS::Parser.parse(rss_content, false)

    rss.items.each do |item|

      title        = item.title
      caption      = item.description
      source       = item.link
      published_at = item.pubDate

        visit(source)
        if article_images.count == 1
          add_result do |result|
            result.title        = title
            result.source       = source
            result.path         = clean_image_url(!article_images[0][:src].blank? ? article_images[0][:src] : article_images[0][:srcset])
            result.caption      = caption
            result.people       = pull_people(title + " " + caption)
            result.published_at = published_at
          end
        elsif article_images.count > 1
          article_images.each_with_index do |image, index|
            add_result do |result|
              result.title        = title
              result.source       = source
              result.path         = clean_image_url(!image[:src].blank? ? image[:src] : image[:srcset])
              result.caption      = caption_for_image(image) || caption
              result.people       = pull_people(title + " " + (caption_for_image(image) || caption))
              result.published_at = published_at
              result.series_root         = source
              result.series_item         = index+1
            end
          end
        end

        if is_slideshow?
        
          enlarge_slideshow
          
          sleep 2
          if session.first('.start-covercard button')
            source = session.current_url
            

            session.all(".carousel-item").each_with_index do |slide,index|
              
             
              current_url = source+"##{index+1}"

              add_result do |result|
                  result.title        = title
                  result.source       = current_url
                  result.path         = clean_image_url(article_slide_image(slide))
                  result.caption      = current_slide_caption_text(slide) || caption
                  result.people       = pull_people(title + " " + caption)
                  result.published_at = published_at
                  result.series_root  = source
                  result.series_item  = index+1
              end
              

              break if last_slide?(index+1)
            end
          end
        else # slide is one page
          source = session.current_url
          article_slides.each_with_index do |slide, index|
            if article_slide_image(slide)
              add_result do |result|
                result.title        = article_slide_title
                result.source       = source
                result.path         = clean_image_url(article_slide_image(slide))
                result.caption      = article_slide_caption(slide) || caption
                result.people       = pull_people(title + " " + caption)
                result.published_at = published_at
                result.series_root  = source
                result.series_item  = index+1
              end
            end
          end
        end
        session.driver.reset!

    end
  end

  # Vanity fair is using dynamic image resizing, so two images of the same article could
  # be duplicated if we don't normalize the dynamic size.
  #
  # Ex:
  #     http://media.vanityfair.com/photos/57d2afc0a9f841aa372f54f5/master/w_790,c_limit/elon-musk-falcon-9.jpg
  #     http://media.vanityfair.com/photos/57d2afc0a9f841aa372f54f5/master/w_768,c_limit/elon-musk-falcon-9.jpg
  #
  # Should both be the same. We'll normalize all widths to 800 for now.
  def clean_image_url(url)
    url.to_s.strip.gsub(/w_(\d*)/i, "w_800")
  end

  def is_slideshow?
    !!session.first('div.slideshow')
  end

  def is_ad?
    !!session.first('div.interstitial')
  end

  def article_images
    begin
      cover  = session.all('.component-article-main-image img').to_a | session.all('.embedded-image img').to_a rescue []
    rescue
      cover = []
    end
      # cover
  end


  def enlarge_slideshow
    session.first('.embed-enlarge').trigger('click')
  end

  def start_slideshow
    debugger
    session.first('.start-covercard button').click
  end

  def current_slide
    session.find('.carousel-item.active')
  end

  def current_slide_image
    img = current_slide.first('img')
    img = !img[:src].blank? ? img[:src] : img[:srcset]
    img
  end

  def current_slide_caption_text(slide)
    slide.find('.caption').try(:text) rescue nil
  end

  def current_slide_num
    current_slide.first('.current').try(:text)
  end

  def total_slides_num
    current_slide.first('.total').try(:text)
  end

  def click_next
    session.find(".carousel-wrap .controls .control.next",visible: false).trigger("click")
    session.find('.start-next').trigger('click')
    sleep 1
  end

  def last_slide?(num)
     num.to_s == total_slides_num
  end

  def caption_for_image(image)
    begin
      image.parent.find('.component-article-main-image').first('figcaption').try(:text)
    rescue
      nil
    end
  end


  def article_slides
    session.all('.listicle-item')
  end

  def article_slide_caption(slide)
    slide.first('.caption').try(:text) rescue nil
  end

  def article_slide_image(slide)
    img = slide.first('img')
    img = img ? !img[:src].blank? ? img[:src] : img[:srcset] : nil
    img
  end

  def article_slide_title
    session.first('.article-header .hed').try(:text)
  end

  def article_published_at
    session.first('.article-header .publish-date').try(:text)
  end

end
