class InStyle < Zig::Crawler
  self.site_key     = "instyle.com"
  self.site_root    = "http://www.instyle.com/celebrity"
  self.max_attempts = 20

  private

  attr_reader :page

  def agent
    @agent ||= Mechanize.new
  end

  def article_image_url
    if img = page.search(".article-wrap .hero-image .image-wrap img").first
      img["src"]
    end
  end

  def article_instagram_refs
    page.search(".article-body .field-body .instagram-media")
  end

  def article_other_photo_urls
    page.search(".article-body .field-body .article-image .image-wrap img").map { |i| i["src"] }.compact
  end

  # Grab any celebrity page links and add them to the people list to tag
  def article_people_refs
    if first_paragraph = page.search(".article-body .field-body p").first
      first_paragraph.search('a').map { |a| a["href"] }.keep_if { |href|
        /instyle\.com\/celebrity/ =~ href
      }.map { |href|
        href.gsub(/^(.*?)instyle.com\/celebrity\/(.*?)$/, '\2')
      }.join(" ")
    end
  end

  def article_publish_date
    if dateline = page.search(".article-body .article-date-author .date-display-single").first
      dateline["content"]
    end
  end

  # Just grab the first bits of the article
  def article_snippet
    page.search(".article-body .field-body p").take(2).map { |p| p.text }.join("\n\n")
  end

  def article_title
    if h1 = page.search(".col1 .article-title").first
      h1.text
    end
  end

  def post_urls
    page.search(".medium-tout .post").take(max_attempts).map do |post|
      if link = post.search(".article-title a").first
        link.attribute("href").value
      end
    end
  end

  def process_pages
    visit(site_root)

    post_urls.each do |source|
      begin
        visit(source)

        url           = article_image_url
        title         = article_title
        caption       = article_snippet
        people        = article_people_refs
        other_photos  = article_other_photo_urls
        instagrams    = article_instagram_refs
        published_at  = article_publish_date
        is_series     = (other_photos.size > 0)

        # make sure this photo is good with a photo url and title
        if url && title
          # then, make sure there is more than just instagrams on this page
          # they seem to re-post a lot of instagrams we already have so no need to dup
          if instagrams.size == 0 || is_series
            add_result do |result|
              result.title        = title
              result.source       = source
              result.path         = url
              result.caption      = caption
              result.published_at = published_at
              result.people       = people

              if is_series
                result.series_root = url
                result.series_item = 0
              end
            end

            if is_series
              other_photos.each_with_index do |other_url, index|
                series_index = (index + 1)

                add_result do |result|
                  result.title        = title
                  result.source       = source
                  result.path         = other_url
                  result.caption      = caption
                  result.published_at = published_at
                  result.people       = people
                  result.series_root  = url
                  result.series_item  = series_index
                end
              end
            end
          end
        end
      rescue Exception => e
        puts "Error in reaching URL: #{e.message}"
      end
    end
  end

  def visit(url)
    @page = agent.get(url)
  end

end
