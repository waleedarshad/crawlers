class PerezHilton < Zig::Crawler
  self.site_key     = 'perezhilton.com'
  self.site_root    = 'http://perezhilton.com/galleries/'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    gallery_urls.each do |url|
      visit(url)

      number_of_slides.times do |index|

        if image(index) && description
          add_result do |result|
            result.title        = title(index)
            result.source       = session.current_url
            result.path         = image(index)[:href]
            result.caption      = description
            result.published_at = published_at
            result.people       = pull_people(result.title + ' ' + result.caption)
            result.series_root  = url
            result.series_item  = index+1
          end
        end

        # click_next_slide
      end
    end
  end

  def gallery_urls
    urls = [session.current_url]
    session.all('a.url').map {|link| urls << link[:href] }.uniq.take(10)
    urls
  end

  def number_of_slides
    session.first('.galleryImageNumber').text.split('/')[1].to_i
  end

  def image(index)
    session.first(".galleryImage a#galleryBigPic_#{index}", :visible => false)
  end

  def title(index)
    image(index)[:title]
  end

  def description
    session.first('h1.galleryTitle').try(:text)
  end

  def published_at
    session.first('.galleryPublishDate').try(:text).to_s.gsub('Published:', '')
  end
end
