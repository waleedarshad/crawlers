class Variety < Zig::Crawler
  self.site_key     = 'variety.com'
  self.site_root    = 'http://variety.com'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    post_urls.each do |url|
      visit(url)
      # sleep 1

      images.each_with_index do |image, index|
        if image
          add_result do |result|
            result.title    = title
            result.source   = session.current_url
            result.path     = image[:src]
            result.caption  = description
            result.published_at  = published_at
            result.people   = pull_people(result.title + ' ' + image[:alt])
            result.series_root = url if images.count > 1
            result.series_item = index+1 if images.count > 1
          end
        end
      end
    end
  end

  def post_urls
    session.all('div#content-flow-top div.col1 article.story a').take(8).map { |link| link[:href] }
  end

  def images
    session.all('div.article-body figure img')
  end

  def title
    session.first('header h1').try(:text)
  end

  def description
    session.first('div.article-body div.variety-content-wrapper p').try(:text)
  end

  def published_at
    session.first('div.article-body time.timestamp')['datetime']
  end
end