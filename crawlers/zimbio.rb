class Zimbio < Zig::Crawler
  self.site_key     = 'zimbio.com'
  self.site_root    = 'http://www.zimbio.com/pictures'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)

    sleep 1

    gallery_urls.each do |url|
      visit(url)

      sleep 1

      number_slides_remaining.times do |index|

        if image && title
          add_result do |result|
            result.title        = title
            result.source       = session.current_url
            result.path         = image[:src]
            result.caption      = image[:alt]
            result.people       = people
            result.published_at = published_at
            result.series_root  = url
            result.series_item  = index+1
          end
        end

        click_next_slide
      end
    end
  end

  def gallery_urls
    session.all('a.thumbnail-item').map { |link| link[:href] }.uniq.take(6)
  end

  def number_of_slides
    session.find('i.rounded-corners').text.split(' of ')[1].to_i
  end

  def number_slides_remaining
    number_of_slides - 1
  end

  def image
    session.first('#currentPic')
  end

  def title
    session.first('.captionText .captionTitleText').try(:text)
  end

  def people
    session.first('#captionPeople a').try(:text)
  end

  def published_at
    session.first('#captionSource').try(:text)
  end

  def click_next_slide
    session.first('a._c.btn-theme.btn-next').try(:click)
  end
end
