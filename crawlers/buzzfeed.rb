class Buzzfeed < Zig::Crawler
  self.site_key     = 'buzzfeed.com'
  self.site_root    = 'https://www.buzzfeed.com/celebrity'
  self.max_attempts = 10

  protected

  def process_pages
    visit(site_root)
    gallery_urls.each do |url|
      visit(url)

      title        = post_title
      caption      = post_description
      source       = session.current_url
      published_at = published_date
      count = 0
      gallery_images.each_with_index do |div, index|

        if image(div)
          add_result do |result|
            result.title        = title
            result.source       = source
            result.path         = image(div)
            result.caption      = count == 0 ? caption : image_caption(div)
            result.published_at = published_at
            result.people       = pull_people([title,caption].join(' '))
            result.series_root  = source
            result.series_item  = count
          end
          count = count+1
        end
      end
    end

  end

  def gallery_urls
    session.all('ul.grid-posts li a.lede__link').map { |link| link[:href] }.uniq
  end

  def gallery_images
    session.all('div.buzz_superlist_item')
  end

  def image(div)
     img = div.first('img')
     if (img['rel:bf_image_src'] rescue nil)
       url = img['rel:bf_image_src'].start_with?('https','http')

       if url
          return nil if img['rel:bf_image_src']['https://twemoji.maxcdn.com']
          return img['rel:bf_image_src']
       end
     end

      return nil

  end

  def post_title
    session.first('h1#post-title').try(:text)
  end

  def post_description
    session.first('p.description').try(:text)
  end

  def published_date
    if text = session.first('span.post-datetimes').try(:text)
      text.to_s.gsub("posted on", "").gsub(".", "").strip
    end
  end

  def image_caption(div)
    text = div.try(:text)
    if text
      text
    else
      caption
    end
  end
end
