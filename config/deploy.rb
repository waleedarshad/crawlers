lock "3.3.5"

set :application, "zig_crawlers"
set :repo_url, "git@github.com:teamzig/crawlers.git"

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }

set :deploy_to, "/var/www/apps/crawl.zig.com"
# set :scm, :git

# set :format, :pretty
set :log_level, :info
# set :pty, true

set :rails_env, :production

set :linked_files, %w( .env cookies.txt )
set :linked_dirs, %w( log )

set :keep_releases, 5

set :whenever_identifier, -> { "#{fetch(:application)}_#{fetch(:stage)}" }
set :whenever_roles, -> { :app }
