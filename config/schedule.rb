# NOTE Server is in UTC time (+6)

set :output, "/var/www/apps/crawl.zig.com/current/log/cron_log.log"

job_type :crawl,  "cd :path && ./bin/zig-crawler :task :output"

# Important!
# every 13.minutes do
#   crawl :instagram
# end

# 5am - 9pm, every half hour on weekdays
every :day, at: "1am,2am,3am,11am,12am,1pm,2pm,3pm,4pm,5pm,6pm,7pm,8pm,9pm,10pm,11pm,12pm" do
  crawl :people_mag
  crawl :us_weekly
  crawl :vogue
  crawl :wetpaint
  crawl :e_online
  crawl :vanity_fair
  crawl :just_jared
  crawl :ew
  crawl :extratv
end

every :day, at: "1:30am,2:30am,3:30am,11:30am,12:30am,1:30pm,2:30pm,3:30pm,4:30pm,5:30pm,6:30pm,7:30pm,8:30pm,9:30pm,10:30pm,11:30pm,1:30pm" do
  crawl :people_mag
  crawl :us_weekly
  crawl :vogue
  crawl :wetpaint
  crawl :e_online
  crawl :vanity_fair
  crawl :buzzfeed
  crawl :celeb_dirty_laundry
  crawl :ny_post
end

# 6am - 8pm, every hour on weekdays
every :day, at: "2am,4am,12pm,2pm,4pm,6pm,8pm,10pm,3am,5am,1pm,3pm,5pm,7pm,9pm,11pm" do
  crawl :popcrush
  crawl :popsugar
  crawl :usa_today
  crawl :zimbio
  crawl :fishwrapper
  crawl :wetpaint
  crawl :pagesix
  crawl :thefrisky
  crawl :ny_daily_news
end

# 7am - 10pm, every 2 hours on weekdays
every :day, at: "2:42am,4:42am,12:42pm,3:42pm,5:42pm,7:42pm,9:42pm,3:15am,5:16am,1:24pm" do
  crawl :perez_hilton
  crawl :tmz
  crawl :vulture
  crawl :yahoo_celeb
  crawl :radar
  crawl :media_takeout
  crawl :wonderwall
  crawl :in_style
  crawl :variety
  crawl :toofab
  crawl :celebuzz
end

# daily at 10:30am
every :day, at: "4:35pm" do
  crawl :cnn
  crawl :fox_news
  crawl :daily_mail
  crawl :ny_daily_news
  crawl :ok_magazine
end

every :day, at: "1:30am,2:30am,3:30am,5:30am" do
  crawl :ask_men
  crawl :eq
  crawl :e_online
  crawl :et_online
  crawl :billboard
end
