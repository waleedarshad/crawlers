require "rubygems"
require "bundler/setup"

require "active_support"
require "active_support/inflector"
require "active_support/core_ext/class/attribute"
require 'active_support/core_ext/object/try'
require 'active_support/core_ext/time'

require "pry"
require "capybara/poltergeist"
require "pp"
require "dotenv"
require "uri"
require "faraday"
# require "instagram"
require "mechanize"

require "optparse"
require "ostruct"

require "./lib/zig-crawlers"
