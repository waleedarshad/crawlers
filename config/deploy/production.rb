set :stage, :production

role :app, %w{deploy@zig.com}

server 'zig.com', user: 'deploy', roles: %w( app )
