require 'rspec'
require 'byebug'
require 'spec_helper'
require 'active_support'
require 'active_support/core_ext'
require_relative "../config/boot"
require_relative "../lib/zig/crawler/attempts"
require_relative "../lib/zig/crawler/session"
require_relative "../lib/zig/crawler"
require_relative "../crawlers/mashable"

describe "Mashable Crawler" do
  it "Return true" do
    mashable = Mashable.new
    mashable.crawl
    mashable.test_results
  end
end

