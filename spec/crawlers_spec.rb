require 'rspec'
require 'byebug'
require 'spec_helper'
require 'active_support'
require 'active_support/core_ext'
require_relative "../config/boot"
require_relative "../lib/zig/crawler/attempts"
require_relative "../lib/zig/crawler/session"
require_relative "../lib/zig/crawler"

crawler_names = Dir.glob("./crawlers/*.rb").map { |path| File.basename(path).gsub(/\.rb$/, '') }

model = ENV['CRAWLER']

if model.nil?
  Zig.app.logger.info "Which crawler do you want to test ? (ex: #{ crawler_names.join(", ") })"
  Zig.app.logger.info " > "

  model = $stdin.gets.chomp
  Zig.app.logger.info " "
end
require_relative "../crawlers/#{model}"

klass_name = ActiveSupport::Inflector.classify(model)
klass = ActiveSupport::Inflector.safe_constantize(klass_name)

result = []
if klass
  instance = klass.new
  result = instance.crawl
else
  Zig.app.logger.error "\n#{ klass_name } Crawler not found, or there was an error loading it"
  exit(1)
end

describe "Rspec Crawler" do
  it "Return true" do
    expect(result.results.count > 0).to be_truthy
  end
end