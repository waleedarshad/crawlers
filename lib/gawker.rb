class Gawker < Zig::Crawler

  def process_pages
    visit(site_root)

    article_urls.each do |url|
      visit(url)
      add_result do |result|
        result.title        = title
        result.source       = session.current_url
        result.path         = image
        result.caption      = caption
        result.published_at = published_at
        result.people       = people
      end
    end
  end

  protected

  def article_urls
    session.all("div.post-list > div.post-wrapper > article.post > header > h1.headline > a").collect{|a| a[:href]}
  end

  def title
    session.first('h1.headline.entry-title').text
  end

  def published_at
    session.first('span.published').text
  end

  def caption
    session.first('p.first-text').text[/.*.?!+(?=\s|$|\z){2}/] || session.first('p.first-text').text
  end

  def people
    session.all('div#taglist', :visible=>false).collect{|li| li.text(:all).titleize}.to_sentence
  end

  def image(node = session)
    session.first('div.post-content > p.has-media img')[:src]
  end

end
