module Zig; module Api
  CouldNotStartCrawlerError = Class.new(StandardError)

  class Client
    def start!(crawler)
      url = '/api/a/crawl'
      response = connection.post(url, site: crawler.site_key)

      unless response.status == 200
        raise CouldNotStartCrawlerError.new("Could not start #{ crawler } crawl #{ response.body }")
      end

      crawl_record = JSON.parse(response.body)
      crawl_record['id']
    end

    def deliver_photo!(photo, options = {})
      photo[:crawl] = options.fetch(:crawl_id)
      photo[:site]  = options.fetch(:crawler).site_key

      url = '/api/a/photos'
      connection.post(url, photo: photo)
    end

    def complete!(crawl_id)
      url = "/api/a/crawl/#{ crawl_id }/complete"
      connection.post(url, {})
    end

    def fail!(crawl_id, message, timeout_error)
      url = "/api/a/crawl/#{ crawl_id }/fail"
      response = connection.post(url, debug: message, timeout: timeout_error)
    end

    private

    def connection
      @connection ||= Connection.shared_instance
    end
  end
end; end
