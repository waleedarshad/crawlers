module Zig; module Api
  class Connection
    attr_reader :auth_token, :root

    module ClassMethods
      def shared_instance
        @shared_instance ||= new
      end
    end
    extend ClassMethods

    def initialize
      @root = ENV["ZIG_BASE_URL"]
      @auth_token = ENV["AUTH_TOKEN"]

      raise "ZIG_BASE_URL and AUTH_TOKEN are required" if auth_token.blank? || root.blank?
    end

    def post(url, contents)
      faraday_client.post do |req|
        req.url(url)
        req.headers['X-Auth-Token'] = auth_token
        req.headers['Content-Type'] = 'application/json'
        req.body = JSON.dump(contents)
      end
    end

    private

    def faraday_client
      @faraday_client ||= Faraday.new(url: root)
    end
  end
end; end
