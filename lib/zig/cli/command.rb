module Zig; module Cli
  class Command
    module ClassMethods
      def find(crawler)
        begin
          require "./crawlers/#{ crawler }"

          klass_name = ActiveSupport::Inflector.classify(crawler)
          ActiveSupport::Inflector.constantize(klass_name).new
        rescue LoadError
          Zig.app.logger.warn "No crawler found for #{ crawler }. Skipping..."
          nil
        end
      end

      def parse_opts!(args)
        options = OpenStruct.new

        OptionParser.new do |opts|
          opts.banner = "zig-crawler {options} [crawler_name]"

          opts.on_tail("-h", "--help", "-H", "Display this help message.") do
            puts opts
            exit
          end

          opts.on("-v", "--[no-]verbose", "Run verbosely") do |v|
            options[:verbose] = v
          end
        end.parse!(args)

        options
      end
    end

    extend ClassMethods
  end
end; end
