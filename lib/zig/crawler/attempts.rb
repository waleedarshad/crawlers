module Zig
  class Crawler
    module Attempts
      extend ActiveSupport::Concern

      included do
        class_attribute :max_attempts

        self.max_attempts = 5
      end

      protected

      def made_attempt!
        @attempts = attempts + 1
      end

      def another_attempt_allowed?
        attempts < max_attempts
      end

      def attempts
        @attempts ||= 0
      end

      def reset_attempts!
        @attempts = 0
      end

      def max_attempts
        self.class.max_attempts
      end
    end
  end
end
