module Zig
  class Crawler
    module Session
      extend ActiveSupport::Concern

      included do
        class_attribute :site_root
      end

      protected

      def site_root
        self.class.site_root
      end

      def session
        @session ||= create_session
      end

      def visit(url)
        visiting(url)

        if session.respond_to?(:visit)
          session.visit(url)
        else
          session.get(url)
        end
      end

      def visiting(url)
        Zig.app.logger.debug "Visiting url: '#{ url }'"
      end

      def create_session
        Capybara::Session.new(:poltergeist)
      end

      def root_uri
        @root_uri ||= URI(site_root)
      end

      def root_url
        "#{ root_uri.scheme }://#{ root_uri.host}"
      end
    end
  end
end
