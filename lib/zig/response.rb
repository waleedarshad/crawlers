module Zig
  class Response
    attr_reader :failure, :message, :results, :timeout_error

    def initialize(results, failure = false, message = nil, timeout_error = false)
      @results = results.map(&:to_hash)
      @failure = failure
      @message = message
      @timeout_error =  timeout_error
    end

    def failure?
      @failure === true
    end
  end
end
