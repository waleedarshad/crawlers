require 'zig/crawler/attempts'
require 'zig/crawler/session'

module Zig
  class Crawler
    include Attempts
    include Session

    class_attribute :site_key

    def crawl
      begin
        process_pages
      rescue => exception
        @response = Response.new(results, true, debug_information_from_exception(exception), exception.is_a?(Capybara::Poltergeist::TimeoutError))
      else
        @response = Response.new(results)
      end

      @response
    end

    protected

    def in_gallery_of_unknown_length(&block)
      reset_attempts!

      while another_attempt_allowed?
        break unless image && description

        if block.arity == 1
          block.call(attempts)
        else
          block.call
        end
      end
    end

    def results
      @results ||= []
    end

    def add_result(&block)
      result = Result.new
      block.call(result)

      Zig.app.logger.debug("\n-----------------------------")
      Zig.app.logger.debug(JSON.pretty_generate(result.to_hash))
      Zig.app.logger.debug("\n-----------------------------\n\n")

      made_attempt!

      results << result
      result
    end

    def debug_information_from_exception(exception)
      debug_text = exception.message + "\n"
      debug_text << exception.backtrace.take(15).join("\n")
      debug_text
    end

    def pull_people(str)
      str.scan(/([A-Z][\w-]*(\s+[A-Z][\w-]*)+)/).map{|i| i.first}.to_sentence
    end

  end
end
