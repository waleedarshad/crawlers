module Zig
  class Result
    attr_accessor :source, :title, :caption, :path, :people, :series_root, :series_item, :published_at
    attr_accessor :width, :height

    def title
      @title || ''
    end

    def caption
      @caption || ''
    end

    def to_hash
      {
        source: source,
        title: title,
        caption: caption,
        path: path,
        people: people,
        series_root: series_root,
        series_item: series_item,
        published_at: published_at,
        width: width,
        height: height
      }
    end
  end
end
