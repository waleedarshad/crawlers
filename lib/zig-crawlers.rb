$:.unshift File.dirname(__FILE__)

Dotenv.load

require 'zig/app'
module Zig
  def self.app
    @app ||= App.new
  end
end

Dir['config/initializers/*.rb'].sort.each do |initializer|
  require File.expand_path(initializer)
end

require 'zig/cli/command'
require 'zig/api/client'
require 'zig/api/connection'
require 'zig/response'
require 'zig/result'
require 'zig/crawler'
