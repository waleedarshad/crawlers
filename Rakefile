require "./config/boot"

desc "Runs a given crawler"
task :crawl do
  crawler = ENV["CRAWLER"] || capture_crawler

  unless crawler_names.include?(crawler)
    Zig.app.logger.error "You need to choose a crawler that exists!"
    exit 1
  end

  require "./crawlers/#{ crawler }"

  klass_name = ActiveSupport::Inflector.classify(crawler)
  klass = ActiveSupport::Inflector.safe_constantize(klass_name)

  if klass
    instance = klass.new
    result = []

    result = instance.crawl

    Zig.app.logger.info "\e[32m #{ result.results.count } result(s) found"
    Zig.app.logger.info "\n\n"

    p result
  else
    Zig.app.logger.error "#{ klass_name } not found in #{ crawler }.rb, or there was an error loading it"
    exit 1
  end
end

def capture_crawler
  Zig.app.logger.info "Which crawler do you want? (ex: #{ crawler_names.join(", ") })"
  Zig.app.logger.info " > "

  result = $stdin.gets.chomp

  Zig.app.logger.info " "

  result
end

def crawler_names
  Dir.glob("./crawlers/*.rb").map do |path|
    File.basename(path).gsub(/\.rb$/, '')
  end
end
